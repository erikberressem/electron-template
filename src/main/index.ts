import { app, BrowserWindow } from 'electron'

import config from '../utils/config'
import getUrl from '../utils/getUrl'

function createWindow(): BrowserWindow {
    const window = new BrowserWindow(config.window)

    window.loadURL(getUrl())

    return window
}

app.on('window-all-closed', () => {
    app.quit()
})

app.whenReady().then(() => {
    createWindow()
})
