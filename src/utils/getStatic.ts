import { resolve } from 'path'

import config from './config'

// see https://github.com/electron-userland/electron-webpack/issues/241#issuecomment-582920906
export default function getStatic(relativePath = ''): string {
    if (config.isProduction) return resolve(__static, relativePath)
    else return new URL(relativePath, window.location.origin).href
}
