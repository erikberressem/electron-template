import { join } from 'path'
import { pathToFileURL } from 'url'

import config from './config'

export default function getUrl(path?: string): string {
    return config.isProduction
        ? pathToFileURL(join(__dirname, `index.html${path ?? ''}`)).href
        : `http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}${path ?? ''}`
}
