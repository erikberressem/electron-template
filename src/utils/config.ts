import 'dotenv-defaults/config'

import { BrowserWindowConstructorOptions } from 'electron'
import * as env from 'env-var'

const isProduction = env.get('NODE_ENV').default('production').asString() === 'production'

const window: BrowserWindowConstructorOptions = {
    alwaysOnTop: isProduction,
    autoHideMenuBar: isProduction,
    fullscreen: isProduction,
    webPreferences: {
        contextIsolation: false,
        nodeIntegration: true,
    },
}

export default {
    isProduction,
    window,
}
