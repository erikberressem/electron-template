# electron-template

## Configuration

Since this app uses the [dotenv-defaults](https://www.npmjs.com/package/dotenv-defaults) library under the hood you can create a `.env` file to override the default environment variables found in the `.env.defaults` file.
